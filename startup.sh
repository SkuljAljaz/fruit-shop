#!/bin/sh

# Run postgresql server
/etc/init.d/postgresql start

# Set default postgres password
sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'secret'"

npm run start:prod