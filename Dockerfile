FROM node:14.17.5

RUN apt-get update \
  && apt-get install -y postgresql postgresql-contrib \
  && apt-get install sudo \
  && apt-get clean

WORKDIR /app

COPY . .

RUN npm install && npm run build

EXPOSE 5432
EXPOSE 3000

ENV DATABASE_HOST=localhost
ENV DATABASE_PORT=5432
ENV DATABASE_USERNAME=postgres
ENV DATABASE_PASSWORD=secret
ENV DATABASE_DATABASE=postgres

ENTRYPOINT ./startup.sh