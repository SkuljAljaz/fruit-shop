export interface IResponse {
  ok: boolean;
  errors?: string[];
  statusCode?: number;
  result?: any;
}
