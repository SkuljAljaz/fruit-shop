import { Test, TestingModule } from '@nestjs/testing';
import { MockDataController } from './mock-data.controller';
import { MockDataService } from './mock-data.service';

describe('MockDataController', () => {
  let controller: MockDataController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MockDataController],
      providers: [MockDataService],
    }).compile();

    controller = module.get<MockDataController>(MockDataController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
