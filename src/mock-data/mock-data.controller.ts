import { Controller, Get } from '@nestjs/common';
import { MockDataService } from './mock-data.service';

@Controller('mock-data')
export class MockDataController {
  constructor(private readonly mockDataService: MockDataService) {}

  @Get('create')
  create() {
    return this.mockDataService.create();
  }
}
