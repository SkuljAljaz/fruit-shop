import { CreateOrderDto } from 'src/order/dto/create-order.dto';
import { OrderStatus } from 'src/order/entities/order.entity';

export const MDorders: CreateOrderDto[] = [
  {
    o_status: OrderStatus.PENDING,
    orderItems: [
      {
        oi_i_uid: 1,
        oi_quantity: 3,
        oi_weight: 600,
      },
      {
        oi_i_uid: 5,
        oi_quantity: 50,
        oi_weight: 3250,
      },
    ],
  },

  {
    o_status: OrderStatus.PENDING,
    orderItems: [
      {
        oi_i_uid: 1,
        oi_quantity: 12,
        oi_weight: 2400,
      },
      {
        oi_i_uid: 3,
        oi_quantity: 43,
        oi_weight: 9460,
      },
      {
        oi_i_uid: 4,
        oi_quantity: 32,
        oi_weight: 2720,
      },
    ],
  },

  {
    o_status: OrderStatus.PENDING,
    orderItems: [
      {
        oi_i_uid: 1,
        oi_quantity: 33,
        oi_weight: 6600,
      },
      {
        oi_i_uid: 2,
        oi_quantity: 75,
        oi_weight: 16500,
      },
      {
        oi_i_uid: 3,
        oi_quantity: 120,
        oi_weight: 37800,
      },
      {
        oi_i_uid: 4,
        oi_quantity: 40,
        oi_weight: 3400,
      },
      {
        oi_i_uid: 5,
        oi_quantity: 777,
        oi_weight: 50505,
      },
    ],
  },
];
