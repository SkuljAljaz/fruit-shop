import { CreateItemDto } from 'src/item/dto/create-item.dto';
import { FruitType } from '../../item/entities/item.entity';

export const MDitems: CreateItemDto[] = [
  {
    i_title: 'Apples Bramley',
    i_type: FruitType.POME,
    i_description:
      'A large, flattish cooking apple, green in appearance but sometimes with specks of red. The flesh is white, juicy and acidic and when boiled it turns into a frothy pulp making it what many consider the best cooking apple of all.',
    i_brand: 'AppleInc',
    i_produced_in: 'Slovenia',
    i_average_weight_per_piece: 200,
    i_average_weight_delta: 20,
    i_price_per_weight: 90,
    i_stock: 1000,
  },

  {
    i_title: 'Starkrimson Pears',
    i_type: FruitType.POME,
    i_description:
      'Named for their brilliant color, Starkrimson Pears make a stunning presentation on any table. Send this hard-to-find pear gift to the gourmet food lover on your list. They’ll appreciate the fine floral aroma, subtle sweetness and smooth texture. The Starkrimson is a great choice for salads or on a buffet where they can show off their lovely color.',
    i_brand: 'PearInc',
    i_produced_in: 'Slovenia',
    i_average_weight_per_piece: 220,
    i_average_weight_delta: 25,
    i_price_per_weight: 120,
    i_stock: 500,
  },

  {
    i_title: 'Ruby Red Grapefruit',
    i_type: FruitType.DRUPE,
    i_description:
      "These plump, thin skinned grapefruit are a blushing red and dripping with nature's own sweet nectar. Bursting with juice and a sweet-tart flavor, they’re seedless and oh so good.",
    i_brand: 'GrapeInc',
    i_produced_in: 'Slovenia',
    i_average_weight_per_piece: 315,
    i_average_weight_delta: 50,
    i_price_per_weight: 150,
    i_stock: 250,
  },

  {
    i_title: 'Sweet Mountain Bing Cherries',
    i_type: FruitType.AGGREGATE_FRUIT,
    i_description:
      "These big, sweet cherries are grown only in a few high-altitude orchards in Idaho. Clear starry nights and cold mountain snowmelt produce sweeter, crisper, more delicious cherries than any you've ever tasted.",
    i_brand: 'CherryInc',
    i_produced_in: 'Slovenia',
    i_average_weight_per_piece: 85,
    i_average_weight_delta: 5,
    i_price_per_weight: 25,
    i_stock: 2000,
  },

  {
    i_title: 'Empress Plums',
    i_type: FruitType.DRUPE,
    i_description:
      'These delicious, juicy, delights are plump perfection and are a tasty treat to beat the Summer heat! Whether you are enjoying them in a flaky pie, a decadent jelly or jam, or a simple Summer salad, our Empress Plums are sure to impress! Our Plums have just a hint of tartness to balance out the sweet in these juicy, eat-over-the-sink delicacies.',
    i_brand: 'PlumInc',
    i_produced_in: 'Slovenia',
    i_average_weight_per_piece: 65,
    i_average_weight_delta: 5,
    i_price_per_weight: 45,
    i_stock: 1500,
  },
];
