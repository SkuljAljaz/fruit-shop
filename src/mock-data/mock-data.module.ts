import { Module } from '@nestjs/common';
import { MockDataService } from './mock-data.service';
import { MockDataController } from './mock-data.controller';
import { ItemService } from 'src/item/item.service';
import { OrderService } from 'src/order/order.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Item } from 'src/item/entities/item.entity';
import { Order } from 'src/order/entities/order.entity';
import { OrderItem } from 'src/order/entities/order-item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Item, Order, OrderItem])],
  controllers: [MockDataController],
  providers: [MockDataService, ItemService, OrderService],
})
export class MockDataModule {}
