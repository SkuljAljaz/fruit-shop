import { Injectable } from '@nestjs/common';
import { ItemService } from 'src/item/item.service';
import { OrderService } from 'src/order/order.service';
import { MDitems } from './mockdata/items';
import { MDorders } from './mockdata/orders';

@Injectable()
export class MockDataService {
  constructor(
    private itemService: ItemService,
    private orderService: OrderService,
  ) {}

  async create() {
    for (let i = 0; i < MDitems.length; i++) {
      const item = MDitems[i];
      await this.itemService.createItem(item);
    }

    for (let i = 0; i < MDorders.length; i++) {
      const order = MDorders[i];
      await this.orderService.createOrder(order);
    }
  }
}
