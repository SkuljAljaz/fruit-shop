import {
  Body,
  Controller,
  Get,
  HttpException,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemStockDto } from './dto/update-item-stock';
import { UpdateItemDto } from './dto/update-item.dto';
import { ItemService } from './item.service';

@Controller('item')
export class ItemController {
  constructor(private readonly itemService: ItemService) {}

  @Get()
  async findItems(@Query() query) {
    const response = await this.itemService.findItems(query);

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Post()
  async createItem(@Body() itemData: CreateItemDto) {
    if (Object.keys(itemData).length <= 0)
      throw new HttpException('No data was provided.', 400);

    const response = await this.itemService.createItem(itemData);

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Patch(':id')
  async updateItem(@Param() params, @Body() itemUpdateData: UpdateItemDto) {
    if (Object.keys(itemUpdateData).length <= 0)
      throw new HttpException('No data was provided.', 400);

    const response = await this.itemService.updateItem(
      params.id,
      itemUpdateData,
    );

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Patch('stock/:id')
  async updateItemStock(
    @Param() params,
    @Body() itemStockData: UpdateItemStockDto,
  ) {
    if (Object.keys(itemStockData).length <= 0)
      throw new HttpException('No data was provided.', 200);

    const response = await this.itemService.updateItemStock(
      params.id,
      itemStockData.i_stock,
    );

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }
}
