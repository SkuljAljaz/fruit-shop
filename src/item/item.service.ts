import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IResponse } from 'src/interfaces/response.interface';
import {
  Between,
  Connection,
  LessThan,
  Like,
  MoreThan,
  QueryRunner,
  Repository,
} from 'typeorm';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';
import { Item } from './entities/item.entity';

@Injectable()
export class ItemService {
  constructor(
    @InjectRepository(Item) private itemRepository: Repository<Item>,
    private connection: Connection,
  ) {}

  async findItems(query): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    const queryOptions = this.filterOptions(query);

    let queryResult;
    try {
      queryResult = await this.itemRepository.find(queryOptions);
    } catch (error) {
      console.error(error);

      response.errors = ['Failed search.'];
      response.statusCode = 500;

      return response;
    }

    response.ok = true;
    response.result = queryResult;

    return response;
  }

  async createItem(itemData: CreateItemDto): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    const { i_title } = itemData;

    let queryResult;
    try {
      queryResult = await this.itemRepository.findOne({ i_title });
    } catch (error) {
      console.error(error);

      response.errors = ['Failed search.'];
      response.statusCode = 500;

      return response;
    }

    if (queryResult) {
      response.errors = ['Item with the same title already exists.'];
      response.statusCode = 400;

      return response;
    }

    let newItem;
    try {
      newItem = await this.itemRepository.save(itemData);
    } catch (error) {
      console.error(error);

      response.errors = ['Creating new item failed.'];
      response.statusCode = 500;

      return response;
    }

    response.ok = true;
    response.result = newItem;

    return response;
  }

  async updateItem(id: number, itemUpdate: UpdateItemDto): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    if ('i_title' in itemUpdate) {
      response.errors = ['Value of i_title can not be changed.'];
      response.statusCode = 400;

      return response;
    }

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      let item;
      try {
        item = await this.itemRepository.findOne({ i_uid: id });
      } catch (error) {
        console.error(error);

        throw {
          message: 'Item search failed.',
          statusCode: 500,
        };
      }

      if (!item) {
        throw {
          message: 'Item with the provided id does not exist.',
          statusCode: 404,
        };
      }

      const toUpdate = {
        i_type: itemUpdate.i_type || item.i_type,
        i_description: itemUpdate.i_description || item.i_description,
        i_brand: itemUpdate.i_brand || item.i_brand,
        i_produced_in: itemUpdate.i_produced_in || item.i_produced_in,
        i_average_weight_per_piece:
          itemUpdate.i_average_weight_per_piece ||
          item.i_average_weight_per_piece,
        i_average_weight_delta:
          itemUpdate.i_average_weight_delta || item.i_average_weight_delta,
        i_price_per_weight:
          itemUpdate.i_price_per_weight || item.i_price_per_weight,
        i_stock: itemUpdate.i_stock || item.i_stock,
      };

      try {
        await this.itemRepository
          .createQueryBuilder()
          .update()
          .set(toUpdate)
          .where('item.i_uid = :id', { id })
          .execute();
      } catch (error) {
        console.error(error);

        throw {
          message: 'Failed update.',
          statusCode: 500,
        };
      }

      response.ok = true;

      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      await queryRunner.rollbackTransaction();

      response.errors = [error.message];
      response.statusCode = error.statusCode;
    } finally {
      await queryRunner.release();

      return response;
    }
  }

  async updateItemStock(
    id: number,
    itemStock: number,
    qr?: QueryRunner,
  ): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    let queryRunner;
    if (qr) {
      queryRunner = qr;
    } else {
      queryRunner = this.connection.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();
    }

    try {
      let item;
      try {
        item = await this.itemRepository.findOne({ i_uid: id });
      } catch (error) {
        console.error(error);

        throw {
          message: 'Item search failed.',
          statusCode: 500,
        };
      }

      if (!item) {
        throw {
          message: 'Item with the provided id does not exist.',
          statusCode: 404,
        };
      }

      const toUpdate = {
        i_stock: itemStock || item.i_stock,
      };

      try {
        await queryRunner.manager
          .getRepository(Item)
          .createQueryBuilder()
          .update()
          .set(toUpdate)
          .where('item.i_uid = :id', { id })
          .execute();
      } catch (error) {
        console.error(error);

        throw {
          message: 'Failed update.',
          statusCode: 500,
        };
      }

      response.ok = true;

      if (!qr) await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      if (!qr) await queryRunner.rollbackTransaction();

      response.errors = [error.message];
      response.statusCode = error.statusCode;
    } finally {
      if (!qr) await queryRunner.release();

      return response;
    }
  }

  filterOptions(query) {
    const queryWhere = {};

    if (query.id) {
      queryWhere['i_uid'] = query.id;
    }

    if (query.title) {
      queryWhere['i_title'] = Like(query.title);
    }

    if (query.type) {
      queryWhere['i_type'] = query.type;
    }

    if (query.brand) {
      queryWhere['i_brand'] = Like(query.brand);
    }

    if (query.produced_in) {
      queryWhere['i_produced_in'] = Like(query.produced_in);
    }

    if (query.min_awpp && query.max_awpp) {
      queryWhere['i_average_weight_per_piece'] = Between(
        query.min_awpp,
        query.max_awpp,
      );
    } else {
      if (query.min_awpp) {
        queryWhere['i_average_weight_per_piece'] = MoreThan(query.min_awpp);
      }

      if (query.max_awpp) {
        queryWhere['i_average_weight_per_piece'] = LessThan(query.max_awpp);
      }
    }

    if (query.min_awd && query.max_awd) {
      queryWhere['i_average_weight_delta'] = Between(
        query.min_awd,
        query.max_awd,
      );
    } else {
      if (query.min_awd) {
        queryWhere['i_average_weight_delta'] = MoreThan(query.min_awd);
      }

      if (query.max_awd) {
        queryWhere['i_average_weight_delta'] = LessThan(query.max_awd);
      }
    }

    if (query.min_ppw && query.max_ppw) {
      queryWhere['i_price_per_weight'] = Between(query.min_ppw, query.max_ppw);
    } else {
      if (query.min_ppw) {
        queryWhere['i_price_per_weight'] = MoreThan(query.min_ppw);
      }

      if (query.max_ppw) {
        queryWhere['i_price_per_weight'] = LessThan(query.max_ppw);
      }
    }

    if (query.min_s && query.max_s) {
      queryWhere['i_stock'] = Between(query.min_s, query.max_s);
    } else {
      if (query.min_s) {
        queryWhere['i_stock'] = MoreThan(query.min_s);
      }

      if (query.max_s) {
        queryWhere['i_stock'] = LessThan(query.max_s);
      }
    }

    const queryOrder = {};

    if (query.ord_id) {
      queryOrder['i_uid'] = query.ord_id;
    }

    if (query.ord_title) {
      queryOrder['i_title'] = query.ord_title;
    }

    if (query.ord_brand) {
      queryOrder['i_brand'] = query.ord_brand;
    }

    if (query.ord_produced_in) {
      queryOrder['i_produced_in'] = query.ord_produced_in;
    }

    if (query.ord_awpp) {
      queryOrder['i_average_weight_per_piece'] = query.ord_awpp;
    }

    if (query.ord_awd) {
      queryOrder['i_average_weight_delta'] = query.ord_awd;
    }

    if (query.ord_ppw) {
      queryOrder['i_price_per_weight'] = query.ord_ppw;
    }

    if (query.ord_s) {
      queryOrder['i_stock'] = query.ord_s;
    }

    if (query.ord_updated_on) {
      queryOrder['i_date_updated_on'] = query.ord_updated_on;
    }

    if (query.ord_created_on) {
      queryOrder['i_date_created_on'] = query.ord_created_on;
    }

    const queryOptions = { where: queryWhere, order: queryOrder };

    if (query.skip) {
      queryOptions['skip'] = query.skip;
    }

    if (query.take) {
      queryOptions['take'] = query.take;
    }

    return queryOptions;
  }
}
