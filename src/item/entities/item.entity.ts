import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// SRC: https://web.fscj.edu/David.Byres/botanynotes/botanych8notes.html
export enum FruitType {
  POME = 'Pome',
  DRUPE = 'Drupe',
  AGGREGATE_FRUIT = 'Aggregate fruit',
  LEGUMES = 'Legumes',
  CAPSULES = 'Capsules',
  NUTS = 'Nuts',
  GRAINS = 'Grains',
  MULTIPLE_FRUITS = 'Multiple fruits',
  NONE = '',
}

@Entity()
export class Item {
  @PrimaryGeneratedColumn()
  i_uid: number;

  @Column({ type: 'varchar', width: 100, unique: true })
  i_title: string;

  @Column({ type: 'enum', enum: FruitType, default: FruitType.NONE })
  i_type: FruitType;

  @Column({ nullable: true, type: 'varchar', width: 500 })
  i_description: string;

  @Column({ nullable: true, type: 'varchar', width: 100 })
  i_brand: string;

  @Column({ nullable: true, type: 'varchar', width: 100 })
  i_produced_in: string;

  @Column({ type: 'int', width: 10 }) // Grams
  i_average_weight_per_piece: number;

  @Column({ type: 'int', width: 10 }) // Grams
  i_average_weight_delta: number;

  @Column({ type: 'int', width: 10 }) // Cents per 1000g
  i_price_per_weight: number;

  @Column({ type: 'int', width: 100, default: 0 })
  i_stock: number;

  @UpdateDateColumn()
  i_date_updated_on: Date;

  @CreateDateColumn()
  i_date_created_on: Date;
}
