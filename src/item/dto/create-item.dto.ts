import { IsNotEmpty } from 'class-validator';
import { FruitType } from '../entities/item.entity';

export class CreateItemDto {
  @IsNotEmpty()
  readonly i_title: string;

  readonly i_type: FruitType;

  readonly i_description: string;

  readonly i_brand: string;

  readonly i_produced_in: string;

  @IsNotEmpty()
  readonly i_average_weight_per_piece: number;

  @IsNotEmpty()
  readonly i_average_weight_delta: number;

  @IsNotEmpty()
  readonly i_price_per_weight: number;

  readonly i_stock: number;
}
