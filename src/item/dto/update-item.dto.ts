import { FruitType } from '../entities/item.entity';

export class UpdateItemDto {
  readonly i_type: FruitType;

  readonly i_description: string;

  readonly i_brand: string;

  readonly i_produced_in: string;

  readonly i_average_weight_per_piece: number;

  readonly i_average_weight_delta: number;

  readonly i_price_per_weight: number;

  readonly i_stock: number;
}
