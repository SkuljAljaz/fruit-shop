import { IsNotEmpty } from 'class-validator';

export class UpdateItemStockDto {
  @IsNotEmpty()
  readonly i_stock: number;
}
