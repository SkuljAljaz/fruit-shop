import { Item } from 'src/item/entities/item.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  oi_uid: number;

  @Column({ type: 'int', width: 10 })
  oi_quantity: number;

  @Column({ type: 'int', width: 10 })
  oi_weight: number;

  @Column({ type: 'int', width: 10 })
  oi_price_per_weight: number;

  @UpdateDateColumn()
  oi_date_updated_on: Date;

  @CreateDateColumn()
  oi_date_created_on: Date;

  @OneToOne(() => Item, { createForeignKeyConstraints: false })
  @JoinColumn()
  oi_i_uid: Item;

  @ManyToOne(() => Order, (order) => order.o_oi_uid, {
    onDelete: 'CASCADE',
  })
  oi_o_uid: Order;
}
