import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './order-item.entity';

export enum OrderStatus {
  COMPLETED = 'COMPLETED',
  PENDING = 'PENDING',
  CANCELED = 'CANCELED',
}

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  o_uid: number;

  @Column({ type: 'enum', enum: OrderStatus, default: OrderStatus.PENDING })
  o_status: OrderStatus;

  @Column({ type: 'int', width: 10 })
  o_total_weight: number;

  @Column({ type: 'int', width: 10 })
  o_total_price: number;

  @UpdateDateColumn()
  o_date_updated_on: Date;

  @CreateDateColumn()
  o_date_created_on: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.oi_o_uid, {
    onDelete: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  o_oi_uid: OrderItem[];
}
