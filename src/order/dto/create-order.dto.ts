import { IsNotEmpty } from 'class-validator';
import { OrderStatus } from '../entities/order.entity';
import { CreateOrderItemDto } from './create-order-item.dto';

export class CreateOrderDto {
  readonly o_status: OrderStatus;

  @IsNotEmpty()
  readonly orderItems: CreateOrderItemDto[];
}
