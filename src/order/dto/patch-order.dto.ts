import { IsNotEmpty } from 'class-validator';
import { OrderStatus } from '../entities/order.entity';

export class PatchOrderDto {
  readonly o_status: OrderStatus;

  readonly orderItems: PatchOrderItemDto[];
}

export class PatchOrderItemDto {
  @IsNotEmpty()
  readonly oi_uid: number;

  readonly oi_i_uid: number;

  readonly oi_quantity: number;

  readonly oi_weight: number;
}
