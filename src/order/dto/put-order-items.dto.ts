import { IsNotEmpty } from 'class-validator';

export class PutOrderDto {
  readonly orderItems: PutOrderItemsDto[];
}

export class PutOrderItemsDto {
  @IsNotEmpty()
  readonly oi_i_uid: number;

  @IsNotEmpty()
  readonly oi_quantity: number;

  @IsNotEmpty()
  readonly oi_weight: number;
}
