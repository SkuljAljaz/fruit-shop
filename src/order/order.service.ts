import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IResponse } from 'src/interfaces/response.interface';
import { Item } from 'src/item/entities/item.entity';
import { ItemService } from 'src/item/item.service';
import { Between, Connection, LessThan, MoreThan, Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { PatchOrderDto } from './dto/patch-order.dto';
import { PutOrderDto } from './dto/put-order-items.dto';
import { OrderItem } from './entities/order-item.entity';
import { Order } from './entities/order.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Item) private itemRepository: Repository<Item>,
    @InjectRepository(Order) private orderRepository: Repository<Order>,
    @InjectRepository(Order) private orderItemRepository: Repository<OrderItem>,
    private itemService: ItemService,
    private connection: Connection,
  ) {}

  async findOrders(query): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    const queryOptions = this.filterOptions(query);

    let queryResult;
    try {
      queryResult = await this.orderRepository.find(queryOptions);
    } catch (error) {
      console.error(error);

      response.errors = ['Failed search.'];
      response.statusCode = 500;

      return response;
    }

    response.ok = true;
    response.result = queryResult;

    return response;
  }

  filterOptions(query) {
    const queryWhere = {};

    if (query.id) {
      queryWhere['o_uid'] = query.id;
    }

    if (query.status) {
      queryWhere['o_status'] = query.status;
    }

    if (query.min_tw && query.max_tw) {
      queryWhere['o_total_weight'] = Between(query.min_tw, query.max_tw);
    } else {
      if (query.min_tw) {
        queryWhere['o_total_weight'] = MoreThan(query.min_tw);
      }

      if (query.max_tw) {
        queryWhere['o_total_weight'] = LessThan(query.max_tw);
      }
    }

    if (query.min_tp && query.max_tp) {
      queryWhere['o_total_price'] = Between(query.min_tp, query.max_tp);
    } else {
      if (query.min_tp) {
        queryWhere['o_total_price'] = MoreThan(query.min_tp);
      }

      if (query.max_tp) {
        queryWhere['o_total_price'] = LessThan(query.max_tp);
      }
    }

    const queryOrder = {};

    if (query.ord_id) {
      queryOrder['o_uid'] = query.ord_id;
    }

    if (query.ord_status) {
      queryOrder['o_status'] = query.ord_status;
    }

    if (query.ord_tw) {
      queryOrder['o_total_weight'] = query.ord_tw;
    }

    if (query.ord_tp) {
      queryOrder['o_total_price'] = query.ord_tp;
    }

    if (query.ord_updated_on) {
      queryOrder['o_date_updated_on'] = query.ord_updated_on;
    }

    if (query.ord_created_on) {
      queryOrder['o_date_created_on'] = query.ord_created_on;
    }

    const queryOptions = { where: queryWhere, order: queryOrder };

    if (query.skip) {
      queryOptions['skip'] = query.skip;
    }

    if (query.take) {
      queryOptions['take'] = query.take;
    }

    return queryOptions;
  }

  async createOrder(orderData: CreateOrderDto): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    const orderItems = orderData.orderItems;

    if (orderItems.length <= 0) {
      response.errors = ['The item order list is empty.'];
      response.statusCode = 400;

      return response;
    }

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      let totalWeight = 0;
      let totalPrice = 0;
      const orderItemArray = [];

      for (let i = 0; i < orderItems.length; i++) {
        const i_uid = orderItems[i].oi_i_uid;
        const itemQuantity = orderItems[i].oi_quantity;

        let itemResult;
        try {
          itemResult = await this.itemRepository.findOne({ i_uid });
        } catch (error) {
          console.log(error);
          response.errors = ['Failed finding item.'];
          response.statusCode = 500;

          return response;
        }

        if (!itemResult) {
          throw {
            message: 'Item with provided id does not exist.',
            statusCode: 404,
          };
        }

        const itemStock = itemResult.i_stock;

        if (itemStock < itemQuantity) {
          throw {
            message: 'Item with provided id does not have enough stock.',
            statusCode: 400,
          };
        }

        const toUpdate = {
          i_stock: itemStock - itemQuantity,
        };

        try {
          await queryRunner.manager
            .getRepository(Item)
            .createQueryBuilder()
            .update()
            .set(toUpdate)
            .where('item.i_uid = :i_uid', { i_uid })
            .execute();
        } catch (error) {
          console.error(error);

          throw {
            message: 'Failed to update item stock.',
            statusCode: 500,
          };
        }

        const orderItem = new OrderItem();
        orderItem['oi_i_uid'] = itemResult;
        orderItem['oi_price_per_weight'] = itemResult.i_price_per_weight;
        orderItem['oi_quantity'] = orderItems[i].oi_quantity;
        orderItem['oi_weight'] = orderItems[i].oi_weight;

        orderItemArray.push(orderItem);

        try {
          await queryRunner.manager.getRepository(OrderItem).save(orderItem);
        } catch (error) {
          console.error(error);

          throw {
            message: 'Failed to create order items.',
            statusCode: 500,
          };
        }

        totalWeight += orderItem.oi_weight;
        totalPrice +=
          (orderItem.oi_price_per_weight / 1000) * orderItem.oi_weight;
      }

      const order = new Order();
      order['o_total_weight'] = totalWeight;
      order['o_total_price'] = Math.round(totalPrice);
      order['o_oi_uid'] = orderItemArray;

      try {
        await queryRunner.manager.getRepository(Order).save(order);
      } catch (error) {
        console.error(error);

        throw {
          message: 'Failed to create order.',
          statusCode: 500,
        };
      }

      response.ok = true;

      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      await queryRunner.rollbackTransaction();

      response.errors = [error.message];
      response.statusCode = error.statusCode;
    } finally {
      await queryRunner.release();

      return response;
    }
  }

  async patchOrder(id, orderData: PatchOrderDto): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      let patchOrder;
      try {
        patchOrder = await this.connection
          .getRepository(Order)
          .createQueryBuilder('order')
          .where('order.o_uid = :id', { id })
          .getOne();
      } catch (error) {
        console.log(error);
        throw {
          message: 'Failed finding order.',
          statusCode: 500,
        };
      }

      if (!patchOrder) {
        throw {
          message: 'Order with provided id does not exist.',
          statusCode: 404,
        };
      }

      const patchOrderItems = orderData.orderItems;

      for (let i = 0; i < patchOrderItems.length || 0; i++) {
        const patchOrderItem = patchOrderItems[i];

        let orderItem;
        try {
          orderItem = await queryRunner.manager
            .getRepository(OrderItem)
            .findOne({
              oi_uid: patchOrderItem.oi_uid,
              oi_o_uid: id,
            });
        } catch (error) {
          console.log(error);

          throw {
            message: 'Failed to fetch order item.',
            statusCode: 500,
          };
        }

        if (!orderItem) {
          throw {
            message:
              'Order item does not exist in the order with the provided id.',
            statusCode: 404,
          };
        }

        const orderItemItemId = patchOrderItem.oi_i_uid || orderItem.oi_i_uid;

        let orderItemItem;
        try {
          orderItemItem = await this.itemRepository.findOne({
            i_uid: orderItemItemId,
          });
        } catch (error) {
          console.log(error);

          throw {
            message: 'Failed to fetch order items item.',
            statusCode: 500,
          };
        }

        if (!orderItemItem) {
          throw {
            message: 'Order items item does not exist.',
            statusCode: 404,
          };
        }

        const stockDelta = orderItem.oi_quantity - patchOrderItem.oi_quantity;

        if (orderItemItem.i_stock < orderItem.oi_quantity - stockDelta) {
          throw {
            message: 'Not enough in stock.',
            statusCode: 404,
          };
        }

        const requiredStock = orderItemItem.i_stock + stockDelta;
        const updateStockRes = await this.itemService.updateItemStock(
          orderItemItemId,
          requiredStock,
          queryRunner,
        );

        if (!updateStockRes.ok) {
          throw {
            message: updateStockRes.errors,
            statusCode: updateStockRes.statusCode,
          };
        }

        try {
          orderItem['oi_i_uid'] = patchOrderItem['oi_i_uid'];
          orderItem['oi_quantity'] = patchOrderItem['oi_quantity'];
          orderItem['oi_weight'] = patchOrderItem['oi_weight'];
          orderItem['oi_price_per_weight'] = orderItemItem.i_price_per_weight;

          await queryRunner.manager.getRepository(OrderItem).save(orderItem);
        } catch (error) {
          console.error(error);

          throw {
            message: 'Failed to update order items.',
            statusCode: 500,
          };
        }
      }

      try {
        const orderItems = await queryRunner.manager
          .getRepository(OrderItem)
          .find({
            oi_o_uid: id,
          });

        let totalWeight = 0;
        let totalPrice = 0;

        for (let i = 0; i < orderItems.length; i++) {
          const orderItem = orderItems[i];

          totalWeight += orderItem.oi_weight;
          totalPrice +=
            (orderItem.oi_price_per_weight / 1000) * orderItem.oi_weight;
        }

        patchOrder['o_total_weight'] = totalWeight;
        patchOrder['o_total_price'] = Math.round(totalPrice);
        patchOrder['o_status'] =
          orderData['o_status'] || patchOrder['o_status'];

        await queryRunner.manager.getRepository(Order).save(patchOrder);
      } catch (error) {
        console.error(error);

        throw {
          message: 'Failed update order.',
          statusCode: 500,
        };
      }

      response.ok = true;

      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      await queryRunner.rollbackTransaction();

      response.errors = [error.message];
      response.statusCode = error.statusCode;
    } finally {
      await queryRunner.release();
      return response;
    }
  }

  async putOrder(id, orderItemsData: PutOrderDto): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      let putOrder;
      try {
        putOrder = await this.connection
          .getRepository(Order)
          .createQueryBuilder('order')
          .where('order.o_uid = :id', { id })
          .getOne();
      } catch (error) {
        console.log(error);
        throw {
          message: 'Failed finding order.',
          statusCode: 500,
        };
      }

      if (!putOrder) {
        throw {
          message: 'Order with provided id does not exist.',
          statusCode: 404,
        };
      }

      let totalWeight = putOrder.o_total_weight;
      let totalPrice = putOrder.o_total_price;

      const putOrderItems = orderItemsData.orderItems;

      for (let i = 0; i < putOrderItems.length || 0; i++) {
        const putOrderItem = putOrderItems[i];

        let existingOrderItem;
        try {
          existingOrderItem = await queryRunner.manager
            .getRepository(OrderItem)
            .createQueryBuilder('order_item')
            .where(
              '(order_item.oi_i_uid = :oi_i_uid AND order_item.oi_o_uid = :oi_o_uid)',
            )
            .setParameters({ oi_i_uid: putOrderItem.oi_i_uid, oi_o_uid: id })
            .getOne();
        } catch (error) {
          console.log(error);

          throw {
            message: 'Failed to fetch order item.',
            statusCode: 500,
          };
        }

        if (existingOrderItem) {
          throw {
            message: 'Order item with the provided oi_i_uid already exists.',
            statusCode: 400,
          };
        }

        let orderItemItem;
        try {
          orderItemItem = await this.itemRepository.findOne({
            i_uid: putOrderItem.oi_i_uid,
          });
        } catch (error) {
          console.log(error);

          throw {
            message: 'Failed to fetch order items item.',
            statusCode: 500,
          };
        }

        if (!orderItemItem) {
          throw {
            message: 'Order items item does not exist.',
            statusCode: 404,
          };
        }

        if (orderItemItem.i_stock < putOrderItem.oi_quantity) {
          throw {
            message: 'Not enough in stock.',
            statusCode: 404,
          };
        }

        const requiredStock = orderItemItem.i_stock - putOrderItem.oi_quantity;

        const updateStockRes = await this.itemService.updateItemStock(
          putOrderItem.oi_i_uid,
          requiredStock,
          queryRunner,
        );

        if (!updateStockRes.ok) {
          throw {
            message: updateStockRes.errors,
            statusCode: updateStockRes.statusCode,
          };
        }

        try {
          const newOrderItem = new OrderItem();
          newOrderItem['oi_i_uid'] = orderItemItem;
          newOrderItem['oi_price_per_weight'] =
            orderItemItem.i_price_per_weight;
          newOrderItem['oi_quantity'] = putOrderItem.oi_quantity;
          newOrderItem['oi_weight'] = putOrderItem.oi_weight;
          newOrderItem['oi_o_uid'] = putOrder;

          totalWeight += putOrderItem.oi_weight;
          totalPrice +=
            (orderItemItem.i_price_per_weight / 1000) * putOrderItem.oi_weight;

          await queryRunner.manager.getRepository(OrderItem).save(newOrderItem);
        } catch (error) {
          console.error(error);

          throw {
            message: 'Failed to create order items.',
            statusCode: 500,
          };
        }
      }

      try {
        putOrder['o_total_weight'] = totalWeight;
        putOrder['o_total_price'] = Math.round(totalPrice);

        await queryRunner.manager.getRepository(Order).save(putOrder);
      } catch (error) {
        console.error(error);

        throw {
          message: 'Failed to update order.',
          statusCode: 500,
        };
      }

      response.ok = true;

      await queryRunner.commitTransaction();
    } catch (error) {
      console.error(error);
      await queryRunner.rollbackTransaction();

      response.errors = [error.message];
      response.statusCode = error.statusCode;
    } finally {
      await queryRunner.release();
      return response;
    }
  }

  async deleteOrder(id): Promise<IResponse> {
    const response: IResponse = {
      ok: false,
      errors: undefined,
      statusCode: undefined,
      result: undefined,
    };

    try {
      const deleteOrder = await this.orderRepository.findOne({ o_uid: id });

      if (!deleteOrder) {
        response.ok = true;

        return response;
      }

      await this.orderRepository.remove(deleteOrder);

      response.ok = true;

      return response;
    } catch (error) {
      console.log(error);
      response.errors = ['Failed deleting item.'];
      response.statusCode = 500;

      return response;
    }
  }
}
