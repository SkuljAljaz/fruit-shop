import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  Patch,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { PatchOrderDto } from './dto/patch-order.dto';
import { PutOrderDto } from './dto/put-order-items.dto';
import { OrderService } from './order.service';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Get()
  async findOrders(@Query() query) {
    const response = await this.orderService.findOrders(query);

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Post()
  async placeOrder(@Body() orderData: CreateOrderDto) {
    if (Object.keys(orderData).length <= 0)
      throw new HttpException('No data was provided.', 400);

    const response = await this.orderService.createOrder(orderData);

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Patch(':id')
  async patchOrder(@Param() params, @Body() orderData: PatchOrderDto) {
    if (Object.keys(orderData).length <= 0)
      throw new HttpException('No data was provided.', 400);

    const response = await this.orderService.patchOrder(params.id, orderData);

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Put(':id')
  async putOrder(@Param() params, @Body() orderItemsData: PutOrderDto) {
    if (Object.keys(orderItemsData).length <= 0)
      throw new HttpException('No data was provided.', 400);

    const response = await this.orderService.putOrder(
      params.id,
      orderItemsData,
    );

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }

  @Delete(':id')
  async deleteOrder(@Param() params) {
    const response = await this.orderService.deleteOrder(params.id);

    if (!response.ok)
      throw new HttpException(response.errors, response.statusCode);

    return response;
  }
}
