import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrderModule } from './order/order.module';
import { ItemModule } from './item/item.module';
import { Item } from './item/entities/item.entity';
import { Order } from './order/entities/order.entity';
import { OrderItem } from './order/entities/order-item.entity';
import { MockDataModule } from './mock-data/mock-data.module';

const dbHost = process.env.DATABASE_HOST;
const dbPort = process.env.DATABASE_PORT;
const dbUsername = process.env.DATABASE_USERNAME;
const dbPassword = process.env.DATABASE_PASSWORD;
const dbDatabase = process.env.DATABASE_DATABASE;

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: dbHost,
      port: Number(dbPort),
      username: dbUsername,
      password: dbPassword,
      database: dbDatabase,
      entities: [Item, Order, OrderItem],
      synchronize: true,
    }),
    OrderModule,
    ItemModule,
    MockDataModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
