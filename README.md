# Namestitev

Ustvaril sem docker sliko, ki vsebuje aplikacijo in podatkovno bazo, ter povezavo med njima.Če uporabljate docker lahko enostavno zaženete sliko 'aljazs/fruit-shop:1.0.0' ([link](https://hub.docker.com/repository/docker/aljazs/fruit-shop)) in izpostavite port 3000 (port aplikacije) in po potrebi 5432 (port podatkovne baze).

```sh
docker pull aljazs/fruit-shop:1.0.0
```

```sh
docker run --name fruit-shop -p 5432:5432 -p 3000:3000 -d aljazs/fruit-shop:1.0.0
```

Druga možnost:

1. Namestitev PostgreSQL podatkovne baze
2. Vpisne podatke za povezavo s podatkovno bazo je potrebno vpisati v okoljske spremenljivke

```
    DATABASE_HOST=localhost
    DATABASE_PORT=5432
    DATABASE_USERNAME=postgres
    DATABASE_PASSWORD=secret
    DATABASE_DATABASE=postgres
```

3. Kloniramo ali forkamo kodo
4. Zaženemo ukaz

```
    npm install
```

5. Zaženemo ukaz

```
    npm start
```

# Endpointi

Za lažje testiranje oz. uporabo lahko uporabimo aplikacijo Insomnia. V mapi na repozitoriju 'fruit-shop/insomnia' je JSON datoteka, katero se da importat v to aplikacijo. Vsebuje vse naslednje endpointe.

Kličemo jih lahko tako da na URL, kjer je gostovana aplikacija dodamo pot in izberemo primeren tip klica.

---

## GET `/mock-data/create`

Ta endpoint ustvari 'dummy' podatke. Ta klic je najbolje narediti čisto na začetku. Če se zahteva ta klic kasneje ni nujno, da se bodo zgeneriranli vsi podatki.

---

## GET `/item`

Ta endpoint pridobi vse predmete oz. sadje, ki se ujemajo z dodano filtracijo v poizvedbenih parametrih. Parametri označeni s 'string\*' se vpišejo v LIKE pravilo v SQL poizvedbi. Parametri, ki imajo na začetku imena 'min', se upošteva pravilo GREATER THAN, pri parametrih z 'max', pa LESS THAN. Parametri z 'ord' na začetku imena pa upoštevajo pravilo ORDER, ki ima dve možni vredosti: 'ASC' ali 'DESC'. 'skip' preskoči določeno število zadetkov, 'take' pa vzame določeno število zadetkov.

```
Primer:
  http://localhost:3000/order?skip=1&take=2

Poizvedbeni parametri:

  | Parametri       | Vrednosti       |
  |-----------------|-----------------|
  | id              | number          |
  | title           | string*         |
  | type            | string          |
  | brand           | string*         |
  | produced_in     | string*         |
  | min_awpp        | number          |
  | max_awpp        | number          |
  | min_awd         | number          |
  | max_awd         | number          |
  | min_ppw         | number          |
  | max_ppw         | number          |
  | min_s           | number          |
  | max_s           | number          |
  | ord_id          | 'ASC' ali 'DESC'|
  | ord_title       | 'ASC' ali 'DESC'|
  | ord_brand       | 'ASC' ali 'DESC'|
  | ord_produced_in | 'ASC' ali 'DESC'|
  | ord_awpp        | 'ASC' ali 'DESC'|
  | ord_awd         | 'ASC' ali 'DESC'|
  | ord_ppw         | 'ASC' ali 'DESC'|
  | ord_s           | 'ASC' ali 'DESC'|
  | ord_updated_on  | 'ASC' ali 'DESC'|
  | ord_created_on  | 'ASC' ali 'DESC'|
  | skip            | number          |
  | take            | number          |

  Okrajšave:
    i_average_weight_per_piece  -> awpp
    i_average_weight_delta      -> awd
    i_price_per_weight          -> ppw
    i_stock                     -> s
```

---

## POST `/item`

Ta endpoint vstavi v tabelo predmetov nov predmet. V telo zahteve (request) vpišemo podatke v JSON formatu. Ključi označeni z zvezdico '\*' so nujno potrebni.

```JSON
Primer z vsemi ključi:
  {
    * "i_title": "custom title",
      "i_description": "description of item",
      "i_brand": "brand of item",
      "i_produced_in": "production of item",
    * "i_average_weight_per_piece": 100,
    * "i_average_weight_delta": 100,
    * "i_price_per_weight": 100,
      "i_stock": 1000
  }
```

---

## PATCH `/items/:id`

Ta endpoint omogoča urejanje predmeta z določenim id-jem.

```JSON
Primer z vsemi ključi:
  {
    "i_description": "description of item",
    "i_brand": "brand of item",
    "i_produced_in": "production of item",
    "i_average_weight_per_piece": 100,
    "i_average_weight_delta": 100,
    "i_price_per_weight": 100,
    "i_stock": 1000
  }
```

---

## PATCH `/items/stock/:id`

Ta endpoint omogoča spreminjanje vrednosti zaloge predmetom z pripetim id-jem. Ključ v telesu je nujno potreben.

```JSON
Primer z vsemi ključi:
  {
    * "i_stock": 1500
  }
```

---

## GET `/order`

Ta endpoint pridobi vsa naročila. Pri tem upošteva dodane poizvedbene parametre. Parametri označeni s 'string\*' se vpišejo v LIKE pravilo v SQL poizvedbi. Parametri, ki imajo na začetku imena 'min', se upošteva pravilo GREATER THAN, pri parametrih z 'max', pa LESS THAN. Parametri z 'ord' na začetku imena pa upoštevajo pravilo ORDER, ki ima dve možni vredosti: 'ASC' ali 'DESC'. 'skip' preskoči določeno število zadetkov, 'take' pa vzame določeno število zadetkov.

```
Primer:
  http://localhost:3000/order?skip=1&take=2

Poizvedbeni parametri:

  | Parametri       | Vrednosti       |
  |-----------------|-----------------|
  | id              | number          |
  | status          | string          |
  | min_tw          | number          |
  | max_tw          | number          |
  | min_tp          | number          |
  | max_tp          | number          |
  | ord_id          | 'ASC' ali 'DESC'|
  | ord_status      | 'ASC' ali 'DESC'|
  | ord_tw          | 'ASC' ali 'DESC'|
  | ord_tp          | 'ASC' ali 'DESC'|
  | ord_updated_on  | 'ASC' ali 'DESC'|
  | ord_created_on  | 'ASC' ali 'DESC'|
  | skip            | number          |
  | take            | number          |

  Okrajšave:
      o_total_weight    -> tw
      o_total_price     -> tp

```

---

## POST `/order`

Ta endpoint v bazo vstavi naročilo in vse predmete oz. izbrano sadje. Ključi označeni z zvezdico '\*' so nujno potrebni.

```JSON
Primer z vsemi ključi:
  {
      "o_status": "COMPLETED",
    * "orderItems": [
      {
    *   "oi_i_uid": 1,
    *   "oi_quantity": 1,
    *   "oi_weight": 100
      },
      {
    *   "oi_i_uid": 2,
    *   "oi_quantity": 1,
    *   "oi_weight": 100
      }
    ]
  }
```

---

## PATCH `/order/:id`

Ta endpoint omogoča urejanje naročila z pripetim id-jem in njegovih predmetov.

```JSON
Primer z vsemi ključi:
  {
    * "orderItems": [
      {
    *   "oi_uid": 1,
    *   "oi_i_uid": 1,
    *   "oi_quantity": 10,
    *   "oi_weight": 100
      },
      {
    *   "oi_uid": 2,
    *   "oi_i_uid": 2,
    *   "oi_quantity": 10,
    *   "oi_weight": 100
      }
    ]
  }
```

---

## PUT `/order/:id`

Ta endpoint omogoča dodajanje predmetov naročilu z pripetim id-jem.

```JSON
Primer z vsemi ključi:
  {
    * "orderItems": [
      {
    *   "oi_i_uid": 1,
    *   "oi_quantity": 10,
    *   "oi_weight": 100
      },
      {
    *   "oi_i_uid": 2,
    *   "oi_quantity": 10,
    *   "oi_weight": 100
      }
    ]
  }
```

---

## DELETE `/order/:id`

Ta endpoint omogoča brisanje naročila in njegovih otrok (predmetov naročila) z pripetim id-jem.

# Struktura podatkovne baze

Aplikacija uporablja podatkovno bazo PostgreSQL. Podatkovna baza je sestavljena iz treh tabel. Prva predstavlja predmet oz. v našem primeru sadje.

## ITEM

```
  i_uid
  i_title
  i_type
  i_description
  i_brand
  i_produced_in
  i_average_weight_per_piece
  i_average_weight_delta
  i_price_per_weight
  i_stock
  i_date_updated_on
  i_date_created_on
```

Druga tabela predstavlja naročilo, ki je z tretjo tabelo v relaciji ena proti mnogo. 'o_oi_uid[ ]' predstavlja id-je tretje tabele (naročenega sadja).

## ORDER

```
  o_uid
  o_status
  o_total_weight
  o_total_price
  o_date_updated_on
  o_date_created_on
  o_oi_uid[]
```

Tretja tabela pa predstavlja naročilo in je s prvo tabelo v relaciji ena proti ena. 'oi_i_uid' je id prve tabele (predmeta / sadja), 'oi_o_uid' je pa id druge tabele (naročila).

## ORDER_ITEM

```
  oi_uid
  oi_quantity
  oi_weight
  oi_price_per_weight
  oi_date_updated_on
  oi_date_created_on
  oi_i_uid
  oi_o_uid
```
